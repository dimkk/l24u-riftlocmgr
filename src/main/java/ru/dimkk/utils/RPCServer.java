package ru.dimkk.utils;

import com.google.common.base.Strings;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.AMQP.BasicProperties;

import java.nio.charset.Charset;


public class RPCServer {


    public static RPCServer getInstance()
    {
        return _instance;
    }
    private static final RPCServer _instance = new RPCServer();

    public void RPCServer() {

    }

    public Thread startNewRpcServerInNewThread(wrapperAction action, String rpcQueueName) {
        if (action == null) return null;
        String mqAddress = Config.RABBITMQ_ADDRESS;
        rpcQueueName = Strings.isNullOrEmpty(rpcQueueName) ? "rpc_queue" : rpcQueueName + "riftEditor";
        Thread t = new Thread(new RPCServerUpTask(action, mqAddress, rpcQueueName));
        t.start();
        return t;
    }

    class RPCServerUpTask implements Runnable{
        private wrapperAction _action;
        private QueueingConsumer _consumer;
        private boolean isRunning = true;
        private Connection _connection;
        private Channel _channel;
        private String _mqAddress;
        private String _rpcQueueName;

        RPCServerUpTask(wrapperAction action, String mqAddress, String rpcQueueName){
            _action = action;
            _mqAddress = mqAddress;
            _rpcQueueName = rpcQueueName;
        }

        public void run() {
            try {
                ConnectionFactory factory = new ConnectionFactory();
                factory.setHost(_mqAddress);
                factory.setUsername(Config.RABBITMQ_LOGIN);
                factory.setPassword(Config.RABBITMQ_PASS);


                _connection = factory.newConnection();

                _channel = _connection.createChannel();

                _channel.queueDeclare(_rpcQueueName, false, false, false, null);

                _channel.basicQos(1);

                _consumer = new QueueingConsumer(_channel);
                _channel.basicConsume(_rpcQueueName, false, _consumer);

                System.out.println(" [x] "+ _action.getActionName() +" Awaiting RPC requests, on queue - " + _rpcQueueName);



            while (isRunning) {
                    String response = "";

                    QueueingConsumer.Delivery delivery = null;
                    try {
                        delivery = _consumer.nextDelivery();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    BasicProperties props = delivery.getProperties();
                    BasicProperties replyProps = new BasicProperties
                            .Builder()
                            .correlationId(props.getCorrelationId())
                            .build();

                    try {
                        String message = new String(delivery.getBody(),"UTF-8");
                        //int n = Integer.parseInt(message);
                        String showMessage = message;
                        if (message.length() > 100) showMessage = message.substring(0, 100);
                        System.out.println(" [.] income (" + showMessage + ")");

                        response = "" + _action.doRealWork(message);
                    }
                    catch (Exception e){
                        System.out.println(" [.] " + e.toString());
                        response = "";
                    }
                    finally {

                        _channel.basicPublish( "", props.getReplyTo(), replyProps, response.getBytes("UTF-8"));//;

                        _channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);

                    }
                }
            }
            catch  (Exception e) {
                e.printStackTrace();
            }
            finally {
                if (_connection != null) {
                    try {
                        _connection.close();
                    }
                    catch (Exception ignore) {}
                }
            }
        }
    }
}
