package ru.dimkk.utils.db;

import l2f.commons.geometry.Polygon;
import l2f.commons.geometry.Shape;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import ru.dimkk.utils.models.Territory;
import ru.dimkk.utils.models.l2Point;
import ru.dimkk.utils.models.player;
import ru.dimkk.utils.models.zone;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by dimkk on 11/26/2016.
 */
public class playerFacade extends Facade {

    public static player create(player p) {
        zone z = p.getCurrentZone();
        if (z != null && zoneFacade.getById(z.getId().toString()) == null) {
            zoneFacade.create(z);
        }
        getds().save(p);
        return p;
    }
    public static player create(int playerId) {
        player p = new player(playerId);
        getds().save(p);
        return p;
    }
    public static player update(player p) {
        zone z = p.getCurrentZone();
        if (z != null && zoneFacade.getById(z.getId().toString()) == null) {
            zoneFacade.create(z);
        }
        remove(getById(p.getId().toString()));
        return create(p);
    }
    public static void remove(player p) {
        player toRemove = getById(p.getId().toString());
        getds().delete(toRemove);
    }

    public static player getById(String id){
        List<player> players = getds().createQuery(player.class).filter("_id = ", new ObjectId(id)).limit(1).asList();
        if (players.size() > 0) {
            return players.get(0);
        } else
            return null;
    }

    public static player getByPlayerId(Integer id){
        List<player> players = getds().createQuery(player.class).filter("playerId = ", id).limit(1).asList();
        if (players.size() > 0) {
            return players.get(0);
        } else
            return create(id);
    }

}
