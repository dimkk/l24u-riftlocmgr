package ru.dimkk.utils.db;

import ru.dimkk.utils.models.incomingMessage;

import java.util.Date;
import java.util.List;

/**
 * Created by dimkk on 11/26/2016.
 */
public class incomingMessagesFacade {
    public static void save(String input) {
        incomingMessage message = new incomingMessage();
        message.setData(input);
        message.setRecieveDate(new Date());
        mdbContext.getInstance().getDatastore().save(message);
    }
    public static incomingMessage getLast(){
        List<incomingMessage> messages = mdbContext.getInstance().getDatastore().createQuery(incomingMessage.class).order("-recieveDate").limit(1).asList();
        if (messages.size() > 0) {
            return messages.get(0);
        } else
            return null;
    }
}
