package ru.dimkk.utils.db;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import ru.dimkk.utils.Config;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimkk on 11/26/2016.
 */
public class mdbContext {
    private static mdbContext _instance = new mdbContext();
    public static mdbContext getInstance(){ return _instance; }
    private final Morphia morphia = new Morphia();

    private Datastore datastore;

    public mdbContext() {
        // tell Morphia where to find your classes
        // can be called multiple times with different packages or classes
        morphia.mapPackage("ru.dimkk.utils.models");
        // create the Datastore connecting to the default port on the local host


//        if (Config.MONGO_ADDRESS.equals("mongo")) {
//            //prod
//            datastore = morphia.createDatastore(new MongoClient(Config.MONGO_ADDRESS), "riftZoneManager");
//        } else {
//            //dev
//            MongoClientURI uri = new MongoClientURI("mongodb://admin:Fucku2u2@"+Config.MONGO_ADDRESS+"/?authSource=admin");
//            //List<MongoCredential> creds = new ArrayList<>();
//            //creds.add(MongoCredential.createCredential("admin","riftZoneManager", "Fucku2u2".toCharArray()));
//            datastore = morphia.createDatastore(new MongoClient(uri), "riftZoneManager");
//        }
        datastore = morphia.createDatastore(new MongoClient(Config.MONGO_ADDRESS), "riftZoneManager");
        datastore.ensureIndexes();
    }

    public Datastore getDatastore() {
        return datastore;
    }
}
