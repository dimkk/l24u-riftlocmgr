package ru.dimkk.utils.db;

import org.mongodb.morphia.Datastore;

/**
 * Created by dimkk on 11/26/2016.
 */
public abstract class Facade {
    static Datastore getds() {
        return mdbContext.getInstance().getDatastore();
    }
}
