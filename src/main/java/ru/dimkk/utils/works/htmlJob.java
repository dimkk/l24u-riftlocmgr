package ru.dimkk.utils.works;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.dimkk.utils.db.zoneFacade;
import ru.dimkk.utils.models.htmlMessage;
import ru.dimkk.utils.models.zone;
import ru.dimkk.utils.wrapperAction;

import java.io.File;
import java.io.IOException;

/**
 * Created by dimkk on 11/26/2016.
 */
public class htmlJob extends wrapperAction {
    public htmlJob(String name) {
        super(name);
    }
    @Override
    public String doWork(String json) throws IOException {
        Gson gson = new GsonBuilder().create();
        htmlMessage m = gson.fromJson(json, htmlMessage.class);
        return htmlJobStatic.getContents(m);
    }
}

