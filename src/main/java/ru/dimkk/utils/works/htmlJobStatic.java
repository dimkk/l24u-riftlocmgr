package ru.dimkk.utils.works;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import ru.dimkk.utils.models.htmlMessage;

import java.io.File;
import java.io.IOException;

public class htmlJobStatic{
    public static String getContents(htmlMessage m) throws IOException {
        return Files.toString(new File("./html/"+m.getLocale()+"/"+m.getTitle()+".htm"), Charsets.UTF_8);
    }
}
