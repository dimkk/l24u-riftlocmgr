package ru.dimkk.utils.works;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.dimkk.utils.db.zoneFacade;
import ru.dimkk.utils.models.zone;
import ru.dimkk.utils.wrapperAction;

/**
 * Created by dimkk on 11/26/2016.
 */
public class zoneJob extends wrapperAction {
    public zoneJob(String name) {
        super(name);
    }
    @Override
    public String doWork(String json) {
        Gson gson = new GsonBuilder().create();
        zone z = gson.fromJson(json, zone.class);
        if (z.type.equals("create")) {
            return gson.toJson(zoneFacade.create(z), zone.class);
        }
        if (z.type.equals("update")){
            return gson.toJson(zoneFacade.update(z), zone.class);
        }
        if (z.type.equals("remove")){
            zoneFacade.remove(z);
        }
        if (z.type.equals("get")){
            return gson.toJson(zoneFacade.get(z.skip, z.size));
        }
        if (z.type.equals("getId")){
            return gson.toJson(zoneFacade.getById(z.getId));
        }
        if (z.type.equals("getRandom")){
            return gson.toJson(zoneFacade.getRandom(), zone.class);
        }
        return "ok";
    }
}