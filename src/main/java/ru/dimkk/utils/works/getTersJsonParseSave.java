package ru.dimkk.utils.works;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import l2f.commons.geometry.Polygon;
import ru.dimkk.utils.db.*;
import ru.dimkk.utils.models.Deserializers.TerritoryDeserializer;
import ru.dimkk.utils.models.Territory;
import ru.dimkk.utils.wrapperAction;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimkk on 11/26/2016.
 */
public class getTersJsonParseSave extends wrapperAction {
    public getTersJsonParseSave(String name) {
        super(name);
    }
    @Override
    public String doWork(String input) {
        GsonBuilder gson = new GsonBuilder();
        Type collectionType = new TypeToken<ArrayList<Territory>>(){}.getType();
        gson.registerTypeAdapter(collectionType, new TerritoryDeserializer());
        List<Territory> result = gson.create().fromJson(input, collectionType);
        zoneFacade.saveZonesFromTerritories(result);
        return "ok";
    }

    public String fromDb() {
        String input = incomingMessagesFacade.getLast().getData();
        GsonBuilder gson = new GsonBuilder();
        Type collectionType = new TypeToken<ArrayList<Territory>>(){}.getType();
        gson.registerTypeAdapter(collectionType, new TerritoryDeserializer());
        List<Territory> result = gson.create().fromJson(input, collectionType);
        zoneFacade.saveZonesFromTerritories(result);
        return "ok";
    }
}
