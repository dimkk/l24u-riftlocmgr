package ru.dimkk.utils.works;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.dimkk.utils.db.playerFacade;
import ru.dimkk.utils.db.zoneFacade;
import ru.dimkk.utils.models.CommunicationObject;
import ru.dimkk.utils.models.zone;
import ru.dimkk.utils.wrapperAction;
import ru.dimkk.utils.zoneEditManager;

/**
 * Created by dimkk on 11/26/2016.
 */
public class communicationJob extends wrapperAction {
    public communicationJob(String name) {
        super(name);
    }
    @Override
    public String doWork(String json) {
        Gson gson = new GsonBuilder().create();
        CommunicationObject object = gson.fromJson(json, CommunicationObject.class);
        if (object.getSection().equals("riftEditor")){
            if (object.getMethod().equals("showZone")){
                return getZoneHtml(object);
            }
            if (object.getMethod().equals("showZones")){
                return zoneEditManager.getInstance().getZonesHtml();
            }
            if (object.getMethod().equals("updateZone")){
                zoneEditManager.getInstance().updateCurrentZone(getPlayerId(object), object.getArgs(), object.getHash());
                //showZone
                return getZoneHtml(object, true);
            }
            if (object.getMethod().equals("removeZone")){
                zoneEditManager.getInstance().removeCurrentZone(playerFacade.getByPlayerId(getPlayerId(object)));
                //showZones
                return zoneEditManager.getInstance().getZonesHtml();
            }
            if (object.getMethod().equals("teleToPoint")){
                return zoneEditManager.getInstance().getZoneForPlayer(object);
            }
            if (object.getMethod().equals("createZone")){
                return zoneEditManager.getInstance().createCurrentZone(playerFacade.getByPlayerId(getPlayerId(object)));
            }
            if (object.getMethod().equals("getRandomZone")){
                return gson.toJson(zoneFacade.getRandom(), zone.class);
            }
            if (object.getMethod().equals("getZoneById")){
                return gson.toJson(zoneFacade.getById(object.getArgs()[0]));
            }
        }

        return "ok";
    }
    private Integer getPlayerId(CommunicationObject object){
        return Integer.parseInt(object.getHashVal("playerId"));
    }
    private String getZoneHtml(CommunicationObject object){
        return zoneEditManager.getInstance().getZoneHtml(getPlayerId(object), object.getArgs());
    }
    private String getZoneHtml(CommunicationObject object, boolean cleanArgs){
        return zoneEditManager.getInstance().getZoneHtml(getPlayerId(object), new String[]{""});
    }
}