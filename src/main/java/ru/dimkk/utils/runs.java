package ru.dimkk.utils;

/**
 * Created by dimkk on 11/25/2016.
 */
public class runs {
    public static void setTimeout(Runnable runnable, int delay){
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }).start();
    }
}
