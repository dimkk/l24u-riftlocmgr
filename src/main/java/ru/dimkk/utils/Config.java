package ru.dimkk.utils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import l2f.commons.configuration.ExProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Config
{


    public static final String LOGIN_CONFIGURATION_FILE = "app.properties";
    private final static Logger _log = LoggerFactory.getLogger(Config.class);

    // it has no instancies
    private Config()
    {
    }

    public final static void load()
    {
        loadConfiguration();
    }


    public static String RABBITMQ_ADDRESS;
    public static String MONGO_ADDRESS;
    public static String RABBITMQ_LOGIN;
    public static String RABBITMQ_PASS;
    public static String MQ_QUEUE_NAME;

    public final static void loadConfiguration()
    {
        ExProperties serverSettings = load(LOGIN_CONFIGURATION_FILE);

        RABBITMQ_ADDRESS = serverSettings.getProperty("RABBITMQ_ADDRESS", "192.168.99.100");
        RABBITMQ_LOGIN = serverSettings.getProperty("RABBITMQ_LOGIN", "");
        RABBITMQ_PASS = serverSettings.getProperty("RABBITMQ_PASS", "");
        MONGO_ADDRESS = serverSettings.getProperty("MONGO_ADDRESS", "192.168.99.100");
        MQ_QUEUE_NAME = serverSettings.getProperty("MQ_QUEUE_NAME", "com");
    }

    public static ExProperties load(String filename)
    {
        return load(new File(filename));
    }

    public static ExProperties load(File file)
    {
        ExProperties result = new ExProperties();

        try
        {
            result.load(file);
        }
        catch (IOException e)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            _log.error(sw.toString());
        }

        return result;
    }

}
