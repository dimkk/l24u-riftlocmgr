package ru.dimkk.utils;

import com.google.common.base.Strings;
import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.dimkk.utils.db.playerFacade;
import ru.dimkk.utils.db.zoneFacade;
import ru.dimkk.utils.models.*;
import ru.dimkk.utils.works.htmlJobStatic;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dimkk on 12/5/2016.
 */
public class zoneEditManager {
    protected static final Logger _log = LoggerFactory.getLogger("zoneEditManager");

    private static zoneEditManager _instance;
    public static zoneEditManager getInstance()
    {
        if (_instance == null)
            _instance = new zoneEditManager();
        return _instance;
    }
    private int skip = 0;
    private int size = 0;
    private final String quickVarName = "currentZone";
    public String getZonesHtml() {
        String result = "Error loading HTML! :(";
        try {
            result = htmlJobStatic.getContents(new htmlMessage("zones", "ru"));//HtmCache.getInstance().getNotNull("diablo/rift/editor/zones.htm", Language.RUSSIAN);
            List<zone> zonesToShow = zoneFacade.get(skip, size); //getZones(skip, size);

            StringBuilder replyMSG = new StringBuilder(200);
            for (zone zone : zonesToShow)
                if (zone != null)
                {
                    replyMSG.append("<tr><td>");
                    replyMSG.append("<a action=\"bypass -h diablo_riftEditor_showZone_").append(zone.getId()).append("\">").append(zone.getTitle()).append("</a>");
                    replyMSG.append("</td><td>");
                    replyMSG.append("<a action=\"bypass -h diablo_riftEditor_goIntoRift_").append(zone.getId()).append("\">").append("Enter").append("</a>");
                    replyMSG.append("</td></tr>");
                }
            result = result.replace("%zones_list%", replyMSG.toString());

        } catch (Exception e) {
            _log.error("error", e);
        }
        return result;
    }

    public String getZoneHtml(int playerId, String[] args){
        String result = "Error loading HTML :(!";//HtmCache.getInstance().getNotNull("diablo/rift/editor/zone.htm", Language.RUSSIAN);
        String Id = "";
        if (args.length > 0) Id = args[0];
        try {
            result = htmlJobStatic.getContents(new htmlMessage("zone", "ru"));
            player localPlayer = playerFacade.getByPlayerId(playerId);
            if (localPlayer == null || localPlayer.getCurrentZone() == null || ((localPlayer.getCurrentZone().getId().toString() != Id) && !Strings.isNullOrEmpty(Id))) localPlayer = getSetZone(Id, localPlayer, playerId);
            zone z = localPlayer.getCurrentZone();
            if (z.getTitle() != null){
                result = result.replace("%zone_name%", z.getTitle());
            }
            if (z.getStartNpcLocation() != null) {
                result = result.replace("%zone_start%", "<a action=\"bypass -h diablo_riftEditor_teleToPoint_start \">" + z.getStartNpcLocation().toString() + "</a>");
            }
            if ( z.getEndNpcLocation() != null) {
                result = result.replace("%zone_end%", "<a action=\"bypass -h diablo_riftEditor_teleToPoint_end \">" + z.getEndNpcLocation().toString() + "</a>");
            }
            if (z.getMobsCount()!= null) {
                result = result.replace("%zone_mobscount%", z.getMobsCount().toString());
            }
            StringBuilder replyMSG = new StringBuilder();
            for (int i = 0; i < z.getPoints().size(); i++) {
                l2Point point = z.getPoints().get(i);
                replyMSG.append("<tr><td>");
                replyMSG.append("<a action=\"bypass -h diablo_riftEditor_teleToPoint_").append(i).append("\">").append(point.toString()).append("</a>");
                replyMSG.append("</td><td>");
                replyMSG.append("<a action=\"bypass -h diablo_riftEditor_updateZone_point_").append(i).append("\">").append("Update").append("</a>");
                replyMSG.append("</td><td>");
                replyMSG.append("<a action=\"bypass -h diablo_riftEditor_updateZone_pointDelete_").append(i).append("\">").append("X").append("</a>");
                replyMSG.append("</td></tr>");
            }
            result = result.replace("%zone_points%", replyMSG.toString());
        } catch (Exception e) {
            _log.error("error", e);
        }
        return result;
    }

    public void updateCurrentZone(Integer playerId, String[] args, HashMap<String, String> hashMap){
        if (args.length == 0) return;
        player p = playerFacade.getByPlayerId(playerId);
        zone z = p.getCurrentZone();
        if (args[0].equalsIgnoreCase("title")){
            if (args.length == 1) return;
            String newTitle = args[1];
            z.setTitle(newTitle);
        }
        if (args[0].equalsIgnoreCase("startNpc")){
            z.setStartNpcLocation(new l2Point(hashMap.get("coords")));
        }
        if (args[0].equalsIgnoreCase("endNpc")){
            z.setEndNpcLocation(new l2Point(hashMap.get("coords")));
        }
        if (args[0].equalsIgnoreCase("mobsCount")){
            if (args.length == 1 || !args[0].equals("mobsCount")) return;
            Integer count = Integer.parseInt(args[1]);
            z.setMobsCount(count);
        }
        if (args[0].equalsIgnoreCase("removeAllPoints")){
            z.setPoints(new ArrayList<l2Point>());
        }
        if (args[0].equalsIgnoreCase("addPoint")){
            z.addPoint(new l2Point(hashMap.get("coords")));
        }
        if (args[0].equalsIgnoreCase("point")){
            if (args.length == 1 || !args[0].equals("point")) return;
            Integer index = Integer.parseInt(args[1]);
            z.getPoints().set(index, new l2Point(hashMap.get("coords")));
        }
        if (args[0].equalsIgnoreCase("pointDelete")){
            if (args.length == 1) return;
            Integer index = Integer.parseInt(args[1]);
            List<l2Point> points = new ArrayList<l2Point>(z.getPoints());
            points.remove(z.getPoints().get(index));
            z.setPoints(points);
        }

        try {
            p.setCurrentZone(zoneFacade.update(z));
        } catch (Exception e) {
            _log.error("error", e);
        }
    }

    public String createCurrentZone(player p){
        zone z = new zone("new zone");
        try {
            z = zoneFacade.create(z);
            p.setCurrentZone(z);
            playerFacade.update(p);
        } catch (Exception e) {
            _log.error("err", e);
        }
        return getZoneHtml(p.getPlayerId(), new String[]{""});
    }
    public void removeCurrentZone(player p){
        zone z = p.getCurrentZone();
        p.setCurrentZone(null);
        playerFacade.update(p);
        zoneFacade.remove(z);
    }

    private player getSetZone(String Id, player p, int playerId) throws Exception {
        if (p == null) p = playerFacade.create(playerId);
        zone z = zoneFacade.getById(Id);
        p.setCurrentZone(z);
        return playerFacade.update(p);
    }

    public String getZoneForPlayer(CommunicationObject object) {
        Gson gson = new GsonBuilder().create();
        player localPlayer = playerFacade.getByPlayerId(Integer.parseInt(object.getHashVal("playerId")));
        if (localPlayer != null && localPlayer.getCurrentZone() != null){
            return gson.toJson(localPlayer.getCurrentZone());
        } else {
            return null;
        }
    }
}
