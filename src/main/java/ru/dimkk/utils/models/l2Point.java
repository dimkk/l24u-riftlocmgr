package ru.dimkk.utils.models;

import l2f.commons.geometry.Shape;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

/**
 * Created by dimkk on 11/25/2016.
 */
@Entity("l2Point")
@Indexes(
        @Index(value = "x", fields = @Field("x"))
)
public class l2Point {
    public l2Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public l2Point(String str) {
        try{
            String[] coords = str.split(",");
            this.x = Integer.parseInt(coords[0]);
            this.y = Integer.parseInt(coords[1]);
            this.z = Integer.parseInt(coords[2]);
        } catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public l2Point(){}

    @Id
    private ObjectId id;

    private int x;
    private int y;
    private int z;

    public ObjectId getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    @Override
    public final String toString()
    {
        return x + "," + y + "," + z ;
    }
}
