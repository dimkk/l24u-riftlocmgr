package ru.dimkk.utils.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dimkk on 11/25/2016.
 */
@Entity("incomingMessages")
@Indexes(
        @Index(value = "recieveDate", fields = @Field("recieveDate"))
)
public class incomingMessage {
    @Id
    private ObjectId id;
    @Property("data")
    private String data;
    @Property("recieveDate")
    private Date recieveDate;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getRecieveDate() {
        return recieveDate;
    }

    public void setRecieveDate(Date recieveDate) {
        this.recieveDate = recieveDate;
    }
}
