package ru.dimkk.utils.models.Deserializers;

import com.google.gson.*;
import l2f.commons.geometry.Polygon;
import ru.dimkk.utils.models.Territory;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimkk on 11/26/2016.
 */
public class TerritoryDeserializer implements JsonDeserializer<List<Territory>> {
    Territory resultTer;
    Polygon temp;
    public List<Territory> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<Territory> result = new ArrayList<Territory>();
        JsonArray obj = json.getAsJsonArray();
        for(JsonElement ter:obj){
            resultTer = new Territory();
            JsonArray includes = ter.getAsJsonObject().get("include").getAsJsonArray();
            JsonObject minzObj = ter.getAsJsonObject().get("min").getAsJsonObject();
            JsonObject maxObj = ter.getAsJsonObject().get("max").getAsJsonObject();
            Integer zMin = minzObj.get("z").getAsInt();
            Integer zMax = maxObj.get("z").getAsInt();
            for (JsonElement incl:includes){
                JsonArray points = incl.getAsJsonObject().get("points").getAsJsonArray();
                for(JsonElement elem:points){
                    temp = new Polygon();
                    Integer x = elem.getAsJsonObject().get("x").getAsInt();
                    Integer y = elem.getAsJsonObject().get("y").getAsInt();
                    temp.add(x, y).setZmin(zMin).setZmax(zMax);
                    resultTer.add(temp);
                }

            }
            result.add(resultTer);
        }
        return result;
    }
}