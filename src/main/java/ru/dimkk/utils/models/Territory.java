package ru.dimkk.utils.models;

import java.util.ArrayList;
import java.util.List;

import l2f.commons.geometry.Point3D;
import l2f.commons.geometry.Shape;
import l2f.commons.util.Rnd;


public class Territory
{
    protected final Point3D max = new Point3D();
    protected final Point3D min = new Point3D();

    private final List<Shape> include = new ArrayList<Shape>(1);
    private final List<Shape> exclude = new ArrayList<Shape>(1);

    public int getS(){
        int result = 0;
        List<Shape> territories = getTerritories();
        for (Shape shape:territories){
            int xl = shape.getXmax() - shape.getXmin();
            int yl = shape.getYmax() - shape.getYmin();
            result = result + (Math.abs(xl)*Math.abs(yl));
        }
        return result;
    }

    public Territory()
    {

    }

    public Territory add(Shape shape)
    {
        if (include.isEmpty())
        {
            max.x = shape.getXmax();
            max.y = shape.getYmax();
            max.z = shape.getZmax();
            min.x = shape.getXmin();
            min.y = shape.getYmin();
            min.z = shape.getZmin();
        }
        else
        {
            max.x = Math.max(max.x, shape.getXmax());
            max.y = Math.max(max.y, shape.getYmax());
            max.z = Math.max(max.z, shape.getZmax());
            min.x = Math.min(min.x, shape.getXmin());
            min.y = Math.min(min.y, shape.getYmin());
            min.z = Math.min(min.z, shape.getZmin());
        }

        include.add(shape);
        return this;
    }

    public Territory addBanned(Shape shape)
    {
        exclude.add(shape);
        return this;
    }

    public List<Shape> getTerritories()
    {
        return include;
    }

    public List<Shape> getBannedTerritories()
    {
        return exclude;
    }

    public boolean isExcluded(int x, int y)
    {
        Shape shape;
        for (int i = 0; i < exclude.size(); i++)
        {
            shape = exclude.get(i);
            if (shape.isInside(x, y))
                return true;
        }
        return false;
    }

    public boolean isExcluded(int x, int y, int z)
    {
        Shape shape;
        for (int i = 0; i < exclude.size(); i++)
        {
            shape = exclude.get(i);
            if (shape.isInside(x, y, z))
                return true;
        }
        return false;
    }

    public int getXmax()
    {
        return this.max.x;
    }

    public int getXmin()
    {
        return this.min.x;
    }

    public int getYmax()
    {
        return this.max.y;
    }

    public int getYmin()
    {
        return this.min.y;
    }

    public int getZmax()
    {
        return this.max.z;
    }

    public int getZmin()
    {
        return this.min.z;
    }


}

