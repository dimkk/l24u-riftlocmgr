package ru.dimkk.utils.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

/**
 * Created by dimkk on 11/25/2016.
 */
@Entity("player")
@Indexes(
        @Index(value = "playerId", fields = @Field("playerId"))
)
public class player {

    public player(Integer playerId) {
        this.setPlayerId(playerId);
    }
    public player() {

    }

    @Id
    private ObjectId id;
    @Property("playerId")
    private Integer playerId;
    @Reference
    private zone currentZone;

    public ObjectId getId() {
        return id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public zone getCurrentZone() {
        return currentZone;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public void setCurrentZone(zone currentZone) {
        this.currentZone = currentZone;
    }

    public void removeCurrentZone(){
        this.currentZone = null;
    }
}
