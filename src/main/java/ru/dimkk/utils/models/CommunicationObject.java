package ru.dimkk.utils.models;

import java.util.HashMap;

/**
 * Created by dimkk on 12/5/2016.
 */
public class CommunicationObject {
    private String section;

    public CommunicationObject(String section, String method, String[] args) {
        this.setSection(section);
        this.setMethod(method);
        this.setArgs(args);
    }

    private String method;
    private String[] args;
    private HashMap<String, String> hash = new HashMap<>();

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public HashMap<String, String> getHash() {
        return hash;
    }
    public String getHashVal(String key) {
        return hash.get(key);
    }

    public void setHash(HashMap<String, String> hash) {
        this.hash = hash;
    }

    public void addHash(String key, String val) {
        this.hash.put(key, val);
    }

    public void removeByKey(String key) {
        this.hash.remove(key);
    }
}
