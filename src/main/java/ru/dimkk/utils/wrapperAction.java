package ru.dimkk.utils;

import ru.dimkk.utils.db.incomingMessagesFacade;

import java.io.IOException;

/**
 * Created by dimkk on 11/25/2016.
 */
public abstract class wrapperAction {
    private String _actionName;
    private String _param;

    public wrapperAction(String name){
        _actionName = name;
    }

    public String doRealWork(String param) throws IOException {
        incomingMessagesFacade.save(param);
        return doWork(param);
    }

    public abstract String doWork(String param) throws IOException;

    public String getActionName() {
        return _actionName;
    }

}
