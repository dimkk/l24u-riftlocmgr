/**
 * Created by dimkk on 11/25/2016.
 */
package ru.dimkk;

import com.google.gson.Gson;
import ru.dimkk.utils.Config;
import ru.dimkk.utils.RPCServer;
import ru.dimkk.utils.models.htmlMessage;
import ru.dimkk.utils.runs;
import ru.dimkk.utils.works.communicationJob;
import ru.dimkk.utils.works.doWork;
import ru.dimkk.utils.works.htmlJob;
import ru.dimkk.utils.works.zoneJob;

import java.io.IOException;

public class main {
    public main(){
        zoneJob zJ = new zoneJob("zoneJob");
        htmlJob hJ = new htmlJob("htmlJob");
        communicationJob cJ = new communicationJob("communicationJob");

        //new RPCServer().startNewRpcServerInNewThread(hJ, "html");
        //new RPCServer().startNewRpcServerInNewThread(zJ, "zone");
        new RPCServer().startNewRpcServerInNewThread(cJ, Config.MQ_QUEUE_NAME);

//        runs.setTimeout(() -> {
//            try {
//                System.out.println(hJ.doWork(new Gson().toJson(new htmlMessage("zones", "ru "))));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            final zone z = new zone("ubertest");
//            final l2Point p = new l2Point(0,0,0);
//            final l2Point p2 = new l2Point(1,1,1);
//            final l2Point p3 = new l2Point(2,2,2);
//            final l2Point p4 = new l2Point(3,3,3);
//            final l2Point p5 = new l2Point(4,4,4);
//            z.addPoint(p)
//                    .addPoint(p2)
//                    .addPoint(p3);
//            z.setStartNpcLocation(p4);
//            z.setEndNpcLocation(p5);
//            z.type = "create";
//            String json = new Gson().toJson(z, zone.class);
//            System.out.println(json);
//            new zoneJob("").doWork(json);
//            zone update = zoneFacade.getLast();
//            update.type = "update";
//            update.setTitle("superupdate");
//            new zoneJob("").doWork(new Gson().toJson(update, zone.class));
//            zone del = zoneFacade.getLast();
//            del.type = "remove";
//            new zoneJob("").doWork(new Gson().toJson(del, zone.class));
//        }, 5000);

    }

    private static final String RPC_QUEUE_NAME = "rpc_queue";
    //docker run -d -p 15672:15672 -p 4369:4369 -p 5671:5671 -p 5672:5672 -p 25672:25672 --name some-rabbit rabbitmq:3-management
    public static void main(String[] args) throws Exception
    {
        Config.load();
        System.out.println(Config.RABBITMQ_ADDRESS);
        new main();
    }



}

