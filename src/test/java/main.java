import com.google.gson.Gson;

import junit.framework.TestCase;
import ru.dimkk.utils.db.zoneFacade;
import ru.dimkk.utils.models.l2Point;
import ru.dimkk.utils.models.zone;
import ru.dimkk.utils.works.zoneJob;

public class main extends TestCase {

    // assigning the values
    protected void setUp(){

    }

    // test method to add two values
    public void testZone(){
        final zone z = new zone("ubertest");
        final l2Point p = new l2Point(0,0,0);
        final l2Point p2 = new l2Point(1,1,1);
        final l2Point p3 = new l2Point(2,2,2);
        final l2Point p4 = new l2Point(3,3,3);
        final l2Point p5 = new l2Point(4,4,4);
        z.addPoint(p)
                .addPoint(p2)
                .addPoint(p3);
        z.setStartNpcLocation(p4);
        z.setEndNpcLocation(p5);
        z.type = "create";
        String json = new Gson().toJson(z, zone.class);
        System.out.println(json);
        new zoneJob("").doWork(json);
        zone update = zoneFacade.getLast();
        update.type = "update";
        update.setTitle("superupdate");
        new zoneJob("").doWork(new Gson().toJson(update, zone.class));
        zone del = zoneFacade.getLast();
        del.type = "remove";
        new zoneJob("").doWork(new Gson().toJson(del, zone.class));
    }
}