gulp = require('gulp');
var shell = require('gulp-shell')

gulp.task('run', function(){
var options = {
    ignoreErrors: true,
    timeout:5000
  };
    return gulp.src('./build/libs/*.jar', {read: false})
    .pipe(shell(['gradlew --stop', 'gradle run'], options));
});

gulp.task('watch', function(){
    return gulp.watch("./src/main/java/**/*", ['run']);
});

gulp.task('default', ['run', 'watch']);